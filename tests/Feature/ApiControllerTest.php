<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use \App\Models\Client;
use \App\Models\User;
use Tests\TestCase;

class ApiControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // public function test_example()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }
    // public function testRequiredFieldsForRegister()
    // {
    //     $this->json('POST', 'api/register', ['Accept' => 'application/json'])
    //         ->assertStatus(200)
    //         ->assertJson([
    //             "message" => "The given data was invalid.",
    //             "errors" => [
    //                 "client_name" => "The client_name field is required.",
    //                 "address1" => "The address2 field is required.",
    //                 "address2" => "The address2 field is required.",
    //                 "city" => "The city field is required.",
    //                 "state" => "The state field is required.",
    //                 "country" => "The country field is required.",
    //                 "phone_no1" => "The phone_no1 field is required.",
    //                 "phone_no2" => "The phone_no2 field is required.",
    //                 "zip" => "The zip field is required.",
    //                 "first_name" => "The first_name field is required.",
    //                 "last_name" => "The last_name field is required.",
    //                 "email" => "The email field is required.",
    //                 "password" => "The password field is required.",
    //                 "confirm_password" => "The confirm_password field is required.",
    //                 "phone" => "The phone field is required.",
    //                 "profile_uri" => "The profile_uri field is required."
    //             ]
    //         ]);
    // }
    // public function testRepeatPassword()
    // {
    //     $userData = [
    //         "name" => "John Doe",
    //         "email" => "doe@example.com",
    //         "password" => "demo12345"
    //     ];

    //     $this->json('POST', 'api/register', $userData, ['Accept' => 'application/json'])
    //         ->assertStatus(422)
    //         ->assertJson([
    //             "message" => "The given data was invalid.",
    //             "errors" => [
    //                 "password" => ["The password confirmation does not match."]
    //             ]
    //         ]);
    // }

    public function testSuccessfulRegistration()
    {  
        $response = $this->post('/api/register',[
            "client_name" => "test client",
            "address1" => "adajan",
            "address2" => "pal",
            "city" => "surat",
            "state" => "gujarat",
            "country" => "india",
            "phone_no1" => "1234567890",
            "phone_no2" => "9876543210",
            "zip" => "395007",
            "first_name" => "first test",
            "last_name" => "last test",
            "email" => "test@gmail.com",
            "password" => "12345678",
            "confirm_password" => "12345678",
            "phone" => "9898989898",
            "profile_uri" => "http://testprofile"
        ]);

        $response->assertStatus(200,'user registered.');
    }
    public function testgetDataWithSorting()
    {
        $response = $this->get('api/getData/?orderBy=id&direction=desc',['Accept' => 'application/json']);
        $response->assertStatus(200,'user data found.');
    }
    public function testgetDataWithFiltering()
    {
        $response = $this->get('api/getData/?filterBy=client_name&filterValue=abc',['Accept' => 'application/json']);
        $response->assertStatus(200,'user data found.');
    }
    public function testgetDataWithSortingAndFiltering()
    {
        $response = $this->get('api/getData/?orderBy=id&direction=desc&filterBy=client_name&filterValue=abc',['Accept' => 'application/json']);
        $response->assertStatus(200,'user data found.');
    }
    public function testgetDataWithoutParams()
    {
        $response = $this->get('api/getData',['Accept' => 'application/json']);
        $response->assertStatus(200,'user data found.');
    }
}

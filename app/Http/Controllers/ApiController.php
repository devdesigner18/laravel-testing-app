<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \App\Models\Client;
use \App\Models\User;
use Validator;
use Hash;
use Illuminate\Support\Facades\Redis;

class ApiController extends Controller
{
    public function register(Request $request)
    {
        $client_name = $request->client_name;
        $address1 = $request->address1;
        $address2 = $request->address2;
        $city = $request->city;
        $state = $request->state;
        $country = $request->country;
        $phone_no1 = $request->phone_no1;
        $phone_no2 = $request->phone_no2;
        $zip = $request->zip;

        // $client_id = $request->client_id;
        $first_name = $request->first_name;
        $last_name = $request->last_name;
        $email = $request->email;
        $password = $request->password;
        $confirm_password = $request->confirm_password;
        $phone = $request->phone;
        $profile_uri = $request->profile_uri;

        $rules = [
             'client_name'   => 'required|alpha',
             'address1'   => 'required',
             'address2'   => 'required',
             'city'   => 'required|alpha',
             'state'   => 'required|alpha',
             'country'   => 'required|alpha',
             'phone_no1'   => 'required|numeric',
             'phone_no2'   => 'required|numeric',
             'zip'   => 'required|numeric',
             'first_name'   => 'required|alpha',
             'last_name'   => 'required|alpha',
             'email'   => 'required|email',
             'password'   => 'required',
             'confirm_password'   => 'required|same:password',
             'phone'   => 'required|numeric',
             'profile_uri'   => 'required',
            ];

        $response = array('response' => '', 'success'=>false);
        $validator = Validator::make($request->all(), $rules);
        
        if($validator->fails()) 
        {
            $response = response()->json(['success' => 0, 'response' => $validator->messages()]);
        }
        else
        {
            $mapquestapi_key = "9ySA6D5u4JuTWs10tqTJYe9UOoggJviJ";
            $latitude = $longitude = 0;
            
            $address = urlencode("$request->address1,$request->address2,$request->city,$request->state,$request->country,$request->zip");

            $geo_decode = file_get_contents("http://open.mapquestapi.com/geocoding/v1/address?key=$mapquestapi_key&location=$address");
            if(!empty($geo_decode)){
                $geo_decode = json_decode($geo_decode, true);
                // dd($geo_decode);
                if(isset($geo_decode['results'][0]) && !empty($geo_decode['results'][0])){
                    $result = $geo_decode['results'][0];
                    if(isset($result['locations'][0]) && !empty($result['locations'][0])){
                        $locations = $result['locations'][0];
                        if(isset($locations['latLng']) && !empty($locations['latLng'])){
                            $latitude = $locations['latLng']['lat'];
                            $longitude = $locations['latLng']['lng'];
                            // dd($locations['latLng']);
                        }
                    }
                }
            }

            $redis = Redis::connection();

            if($redis->ping())
            {
                $redis->set('location_lat_long', json_encode([
                    'latitude' => $latitude, 
                    'longitude' => $longitude
                    ])
                );
                
                // $response = $redis->get('location_lat_long');

                // return json_decode($response);
            }

            $client = new Client;
            $client->client_name = $client_name;
            $client->address1 = $address1;
            $client->address2 = $address2;
            $client->city = $city;
            $client->state = $state;
            $client->country = $country;
            $client->phone_no1 = $phone_no1;
            $client->phone_no2 = $phone_no2;
            $client->zip = $zip;
            
            $client->latitude = $latitude;
            $client->longitude = $longitude;
            $client->start_validity = date('Y-m-d');
            $client->end_validity = date('Y-m-d',strtotime('+14 days'));
            $client->status = 'Active';

            if($client->save())
            {
                $profile_uri = time().'.'.$request->profile_uri->extension();
                $request->profile_uri->move(public_path('Profiles'), $profile_uri);

                $user = new User;
                $user->client_id = $client->id;
                $user->first_name = $first_name;
                $user->last_name = $last_name;
                $user->email = $email;
                $user->password = Hash::make($password);
                $user->phone = $phone;
                $user->profile_uri = $profile_uri;
                $user->last_password_reset = date('Y-m-d H:i:s');
                $user->status = 'Active';

                if($user->save())
                {
                    $clientdata = Client::select('client_name as clientName','address1','address2','city','state','country','phone_no1 as phoneNo1','phone_no2 as phoneNo2','zip')
                                ->where('id',$client->id)
                                ->first();

                    $clientdata->user = User::select('first_name as firstName','last_name as lastName','email','password','phone','profile_uri as profileImage')
                            ->where('client_id',$client->id)
                            ->first();

                    $response = response()->json(['success' => 1, 'message' => 'Client data saved successfully', 'response' => $clientdata]);
                }
                else
                {
                    $response = response()->json(['success' => 0, 'response' => 'Something is wrong']);
                }
            }
            else
            {
                $response = response()->json(['success' => 0, 'response' => 'Something is wrong']);
            }
        }
        
        return $response;
    }

    public function getData(Request $request)
    {
        //sorting
        $orderBy = $request->orderBy;
        $direction = $request->direction;

        //filtering
        $filterBy = $request->filterBy;
        $filterValue = $request->filterValue;

        if($orderBy != '' && $direction != '' && $filterBy != '' && $filterValue != '')
        {
            //query
            $alldata = Client::select('id','client_name as clientName','address1','address2','city','state','country','latitude','longitude','phone_no1 as phoneNo1','phone_no2 as phoneNo2','zip','start_validity as startValidity','end_validity as endValidity','status','created_at as createdAt','updated_at as updatedAt')
                    ->where($filterBy,$filterValue)
                    ->orderBy($orderBy,$direction)
                    ->paginate(5);
        }

        elseif($filterBy != '' && $filterValue != '' && $orderBy == '' && $direction == '')
        {
            //query
            $alldata = Client::select('id','client_name as clientName','address1','address2','city','state','country','latitude','longitude','phone_no1 as phoneNo1','phone_no2 as phoneNo2','zip','start_validity as startValidity','end_validity as endValidity','status','created_at as createdAt','updated_at as updatedAt')
                    ->where($filterBy,$filterValue)
                    ->paginate(5);
        }

        elseif($orderBy != '' && $direction != '' && $filterBy == '' && $filterValue == '')
        {
            //query
            $alldata = Client::select('id','client_name as clientName','address1','address2','city','state','country','latitude','longitude','phone_no1 as phoneNo1','phone_no2 as phoneNo2','zip','start_validity as startValidity','end_validity as endValidity','status','created_at as createdAt','updated_at as updatedAt')
                    ->orderBy($orderBy,$direction)
                    ->paginate(5);
        }
        
        elseif($orderBy == '' && $direction == '' && $filterBy == '' && $filterValue == '')
        {
            //query
            $alldata = Client::select('id','client_name as clientName','address1','address2','city','state','country','latitude','longitude','phone_no1 as phoneNo1','phone_no2 as phoneNo2','zip','start_validity as startValidity','end_validity as endValidity','status','created_at as createdAt','updated_at as updatedAt')
                    ->paginate(5);
        }    

        if($alldata->toArray())
        {
            foreach($alldata as $val)
            {
                $val->userData = User::select('first_name as firstName','last_name as lastName','email','phone','profile_uri as profileImage')->where('client_id',$val->id)->first();
            }

            $response = response()->json(['success' => 1, 'response' => $alldata]);
        }
        else
        {
            $response = response()->json(['success' => 0, 'response' => 'No data found']);
        }

        return $response;
    }
}
